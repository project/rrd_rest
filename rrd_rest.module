<?php

/**
 * @file
 * This module provides an interface to RRDTool and Collectd
 * This module will provide api calls for graphs, data points
 * using the RRDTool graph, RRDTool fetch, RRDTool xport
 */

//RRD Tool Executable and data directory
define ('RRD_REST_DATA_DIR', variable_get('rrd_rest_data_dir', '/var/lib/collectd/rrd'));
define ('RRD_REST_RRDTOOL', variable_get('rrd_rest_rrdtool', '/usr/bin/rrdtool'));
define ('RRD_REST_PATTERN_MATCH', '#([\w_]+)(?:\-(.+))?/([\w_]+)(?:\-(.+))?\.rrd#');

//Image Presets
define ('RRD_REST_LARGE', 'large');
define ('RRD_REST_MEDIUM', 'medium');
define ('RRD_REST_SMALL', 'small');

//user parameters
define ('RRD_COMPUTED', 'computed');
define ('RRD_FULL', 'full');
define ('RRD_AVERAGE', 'AVERAGE');

//Response Error Codes
define ('RRD_REST_INVALID_RRD_DIRECTORY', 'INVALID_RRD_DIRECTORY');
define ('RRD_REST_INVALID_HOST', 'INVALID_HOST');
define ('RRD_REST_INVALID_PARAMETERS', 'INVALID_PARAMETERS');
define ('RRD_REST_NO_HOST', 'NO_HOST');
define ('RRD_REST_NO_PLUGIN_DEFINED', 'NO_PLUGIN_FUNCTION_DEFINED');
define ('RRD_REST_NO_PLUGIN', 'NO_PLUGIN');

/**
 * Implementation of hook_menu().
 */
function rrd_rest_menu() {
   
  $items['monitor-server/status'] = array(
    'title' => '',
    'page callback' => 'rrd_rest_status',
    'access arguments' => array('access monitoring dashboard'),
  );
  $items['monitor-server/get-all-hosts'] = array(
    'title' => '',
    'page callback' => 'rrd_rest_get_all_hosts',
    'access arguments' => array('access monitoring dashboard'),
  );
  $items['monitor-server/get-supported-plugins'] = array(
      'title' => '',
      'page callback' => 'rrd_rest_get_supported_plugins',
      'access arguments' => array('access monitoring dashboard'),
  );
  $items['monitor-server/get-plugins-by-host'] = array(
    'title' => '',
    'page callback' => 'rrd_rest_get_plugins_by_host',
    'access arguments' => array('access monitoring dashboard'),
  );
  $items['monitor-server/get-graph-by-plugin'] = array(
    'title' => '',
    'page callback' => 'rrd_rest_get_graph',
    'page arguments' => array(2),
    'access arguments' => array('access monitoring dashboard'),
  );
  $items['monitor-server/get-data-by-plugin'] = array(
    'title' => '',
    'page callback' => 'rrd_rest_get_plugin_data',
    'access arguments' => array('access monitoring dashboard'),
  );
  $items['admin/config/system/rrd_api'] = array(
    'title' => 'RRD Configuration',
    'description' => 'Configure RRD Tool',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('rrd_rest_admin_form'),
    'access arguments' => array('administer site configuration'),
    'file' => 'rrd_rest.pages.inc',
    'type' => MENU_NORMAL_ITEM,
  );
  $items['monitor-server/dashboard'] = array(
    'title' => 'Monitoring Dashboard',
    'description' => 'Sample usage of this api',
    'page callback' => 'rrd_rest_dashboard',
    'access arguments' => array('access monitoring dashboard'),
    'file' => 'rrd_rest.pages.inc',
    'type' => MENU_NORMAL_ITEM,
  );
  $items['monitor-server/dashboard-graph'] = array(
    'title' => 'Graph Details',
    'description' => 'Graph Details',
    'page callback' => 'rrd_rest_dashboard_graph',
    'access arguments' => array('access monitoring dashboard'),
    'file' => 'rrd_rest.pages.inc',
    'type' => MENU_CALLBACK,
  );
  return $items;
}

/**
 * Implements hook_permission(). 
 */
function rrd_rest_permission() {
  return array(
    'access monitoring dashboard' => array(
      'title' => t('Access monitoring dashboard'),
      'description' => t('Access the RRD monitoring dashboard and the graphs that are generated.'),
    ),
  );
}

/**
 * Checks on the status of the server. Makes sure 
 * the RRD Data directory is available. Makes sure 
 * rrdtool is available
 */
function rrd_rest_status() {
  $data = array();
  if (is_dir(RRD_REST_DATA_DIR)) {
    $data['rrd_data_dir'] = RRD_REST_DATA_DIR;
  }
  else {
    $data['rrd_data_dir'] = t('Not Available');
  }
  
  //check on RRD_REST_RRDTOOL
  $return = shell_exec('which ' . RRD_REST_RRDTOOL);
  if (is_null($return)) {
    $data['rrd_tool'] = t('Not Available');
  }
  else {
    $data['rrd_tool'] = trim($return);
  }
  $ip = shell_exec("/sbin/ifconfig  | grep 'inet addr:'| grep -v '127.0.0.1' | cut -d: -f2 | awk '{ print $1}'");
  if (!is_null($ip)) {
    $data['server_ip'] = trim($ip);
  }
  drupal_json_output($data);
}



/**
 * Gets a list of all the hosts
 */
function rrd_rest_get_all_hosts() {
  $dir = _rrd_rest_collectd_hosts();
  $output = '';
  if ($dir == FALSE) {
    $output = _rrd_rest_error(RRD_REST_INVALID_RRD_DIRECTORY);
  }
  else {
    $output =  $dir;
  }
  drupal_json_output($output);
}

/**
 * Gets all the plugins by host 
 */
function rrd_rest_get_plugins_by_host() {
  $params = _rrd_rest_parse_params();
  $output = array();
  if (is_null($params['host'])) {
    $output = _rrd_rest_error(RRD_REST_NO_HOST);
  }
  else {
    $data = _rrd_rest_collectd_plugin_info($params['host'], is_null($params['plugin']) ? '' : $params['plugin'], $params['details']);
    if ($data != FALSE) {
      $output = $data;
    }
    else {
      $output = _rrd_rest_error(RRD_REST_INVALID_PARAMETERS);
    }
  }
  drupal_json_output($output);
}

/**
 * List all the graph types supported by this api
 */
function rrd_rest_get_supported_plugins() {
  module_load_include('inc', 'rrd_rest', 'rrd_rest_plugins');
  $supported_plugins = rrd_rest_supported_plugins();
  drupal_json_output($supported_plugins);
}


/**
 * Get data via RRDTool Fetch. Two modes are 
 * supported.  Computed and Full
 * Computed returns average, max, and min
 * Full returns all values in a given time period
 */
function rrd_rest_get_plugin_data() {
  $params = _rrd_rest_parse_params();
  $output = array();
  if (is_null($params['host'])) {
    $output = _rrd_rest_error(RRD_REST_NO_HOST);
  }
  elseif (is_null($params['plugin'])) {
    $output = _rrd_rest_error(RRD_REST_NO_PLUGIN);
  }
  else {
    module_load_include('inc', 'rrd_rest', 'rrd_rest_plugins');
    $func = 'rrd_rest_plugin_' . $params['plugin'];
    if (function_exists($func)) {
      $config = array(
        'datadir' => RRD_REST_DATA_DIR,
        'rrdtool' => RRD_REST_RRDTOOL,
        'args' => $params,
        'rrdtool_opts' => '',
      );
      $obj = $func($config);
      
      $data = $obj->rrd_gen_data();

      $output = $data;

    }
    if (!is_null($params['format']) && $params['format'] == 'csv') {
      //setup as csv and exit
      drupal_add_http_header('Content-Type', 'text/csv');
      print implode("\r\n", $data);
      exit();
    }
    else {
      $output = $data;
    }
  }
  drupal_json_output($output);
}

/**
 * Main graphing function.  This calls the appropriate config
 * and returns a graph
 */
function rrd_rest_get_graph() {
  $params = _rrd_rest_parse_params();
  $output = '';
  if (isset($params['plugin'])) {
    module_load_include('inc', 'rrd_rest', 'rrd_rest_plugins');
    $func = 'rrd_rest_plugin_' . $params['plugin'];
    if (function_exists($func)) {
      $config = array(
        'datadir' => RRD_REST_DATA_DIR,
        'rrdtool' => RRD_REST_RRDTOOL,
        'args' => _rrd_rest_parse_params(),
        'rrdtool_opts' => '',
      );
      $obj = $func($config);
      $graph = $obj->rrd_graph();
      if ($params['debug'] == TRUE) {
        drupal_json_encode($graph);
      }
      else {
        drupal_add_http_header('Content-Type', 'image/png');
        print `$graph`;
        exit;
      }
    }
    else {
      $output = _rrd_rest_error(RRD_REST_NO_PLUGIN_DEFINED);
    }
  }
  else {
    $output = _rrd_rest_error(RRD_REST_NO_PLUGIN);
  }
  drupal_json_output($output);
}

/**
 * Helper function to generate a json error
 * message 
 */
function _rrd_rest_error($msg) {
  return array('error' => $msg);
}

/**
 * Returns all the collectd hosts being tracked
 */
function _rrd_rest_collectd_hosts() {
  if (!_rrd_rest_is_dir(RRD_REST_DATA_DIR)) {
    return FALSE;
  }

  $dir = scandir(RRD_REST_DATA_DIR);
  $servers = array();
  foreach ($dir as $key => $value) {
    if (_rrd_rest_is_dir(RRD_REST_DATA_DIR . '/' . $value) && $value != '.' && $value != '..') {
      $servers[] = $value;
    }
  }
  return $servers;
}

/**
 * Returns all the plugins being tracked per host.
 * If $detail = FALSE, return the high level plugins. The
 * details such as plugin_instance, type and type_instance
 * will not be returned in that case.
 */
function _rrd_rest_collectd_plugin_info($host, $plugin = '', $details = TRUE) {
  
  if (!_rrd_rest_is_dir(RRD_REST_DATA_DIR . '/' . $host)) {
    return FALSE;
  }
  
  $files = glob(RRD_REST_DATA_DIR . '/' . $host . "/$plugin*/*.rrd");
  $data = array();
  $supported_plugins = rrd_rest_supported_plugins();
  
  if ($details == TRUE) {
    foreach ($files as $item) {
      //whack off the suffix first
      $item = str_replace(RRD_REST_DATA_DIR . '/' . $host . '/', '', $item);
      preg_match(RRD_REST_PATTERN_MATCH, $item, $matches);
      if (in_array($matches[1], $supported_plugins)) {
        $data[] = array(
          'plugin' => $matches[1],
          'plugin_instance' => isset($matches[2]) ? $matches[2] : '',
          'type' => $matches[3],
          'type_instance' => isset($matches[4]) ? $matches[4] : '',
        );
      }
    }
    //massage the data
    $data = _rrd_rest_group_data($data);
  }
  else {
    //only build the plugins and nothing else
    foreach ($files as $file) {
      $file = str_replace(RRD_REST_DATA_DIR . '/' . $host . '/', '', $file);
      preg_match(RRD_REST_PATTERN_MATCH, $file, $matches);
      if (in_array($matches[1], $supported_plugins)) {
        $data[$matches[1]] = $matches[1];
      }
    }
    $data = array_values($data);
  }

  return $data;
}

/**
 * Helper function that checks if a directory exits 
 */
function _rrd_rest_is_dir($dir) {
  $is_dir = FALSE;
  if (is_dir($dir)) {
    $is_dir = TRUE;
  }
  return $is_dir;
}

/**
 * Get the required GET parameters needed for graph
 * and data plugin functions
 */
function _rrd_rest_parse_params() {
  
  $host = isset($_GET['host']) ? $_GET['host'] : NULL;
  $plugin = isset($_GET['plugin']) ? $_GET['plugin'] : NULL;
  $plugin_instance = isset($_GET['plugin_instance']) ? $_GET['plugin_instance'] : NULL;
  $type = isset($_GET['type']) ? $_GET['type'] : NULL;
  $type_instance = isset($_GET['type_instance']) ? $_GET['type_instance'] : NULL;
  $image_preset = isset($_GET['image_preset']) ? $_GET['image_preset'] : RRD_REST_MEDIUM;
  $seconds = isset($_GET['seconds']) && is_numeric($_GET['seconds']) ? (int) $_GET['seconds'] : 86400;
  $data_set = isset($_GET['data_set']) ? $_GET['data_set'] : RRD_COMPUTED;  //default to computed
  $function = isset($_GET['function']) ? $_GET['function'] : RRD_AVERAGE;
  $format = isset($_GET['format']) ? $_GET['format'] : NULL;
  $debug = isset($_GET['debug']) ? $_GET['debug'] : FALSE;
  
  $details = isset($_GET['details']) && !empty($_GET['details']) ? $_GET['details'] : TRUE;
  $details = filter_var($details, FILTER_VALIDATE_BOOLEAN);
  
  $params = array(
    'host' => $host,
    'plugin' => $plugin,
    'plugin_instance' => $plugin_instance,
    'type' => $type,
    'type_instance' => $type_instance,
    'seconds' => $seconds,
    'data_set' => $data_set,
    'function' => $function,
    'format' => $format,
    'debug' => $debug,
    'details' => $details,
  );

  $params = array_merge($params, _rrd_rest_image_presets($image_preset));
  return $params;
}

/**
 * Function defines image presets to generate. This is a 
 * helper function that minimizes GET parameter passing.
 * The following presets are defined.
 *  LARGE = 800x350
 *  MEDIUM = 400x175
 */
function _rrd_rest_image_presets($image_preset) {
  $image_size = array();
  switch ($image_preset) {
    case RRD_REST_LARGE:
      $image_size['width'] = 800;
      $image_size['height'] = 350;
      break;
    case RRD_REST_SMALL:
      $image_size['width'] = 300;
      $image_size['height'] = 150; 
      break;
    case RRD_REST_MEDIUM:
    default:
      $image_size['width'] = 400;
      $image_size['height'] = 175;
      break;
  }
  return $image_size;
}

/**
* group the raw plugin list from the rrd directory
*/
function _rrd_rest_group_data($plugin_info) {
  $data = array();

  if ($plugin_info) {
    foreach ($plugin_info as $value) {
      if (!preg_match('/^(df|interface)$/', $value['plugin'])) {
        unset($value['type_instance']);
      }
      $data[] = $value;
    }
    $data = array_map('unserialize', array_unique(array_map("serialize", $data)));
  }
  return $data;
}

/**
* Return a list of the supported plugins. The graphing
* and data api calls use this function to check whether
* the plugins in this file are defined.
*/
function rrd_rest_supported_plugins() {
  return array(
    'cpu',
    'df',
    'disk',
    'interface',
    'load',
    'memory',
    'processes',
    'swap',
  );
}
